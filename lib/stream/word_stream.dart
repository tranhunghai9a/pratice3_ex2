import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import"package:flutter/material.dart";
import "../model/imageModel.dart";
import "../ui/imageList.dart";

class ImageLinkStream {
  List<ImageModel> imageList = [];

  fetchImageLink() {
    Uri url = Uri.parse("https://jsonplaceholder.typicode.com/photos");
    http.get(url).then((result){
      var jsonList = json.decode(result.body);
      imageList = List<ImageModel>.from(
          jsonList.map((jsonObj) => ImageModel.fromJson(jsonObj)));
    });
  }

  ImageLinkStream() {
    fetchImageLink();
  }

  Stream<ImageModel?> getImageLink() async* {
    yield* Stream.periodic(const Duration(seconds: 5), (int t) {
      ImageModel? imageModel;
      final _random = Random();
      imageModel = imageList[_random.nextInt(imageList.length)];
      return imageModel;
    });
  }
}


