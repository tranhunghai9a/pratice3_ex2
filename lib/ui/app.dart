import 'package:buoi2_bai2/model/imageModel.dart';
import 'package:flutter/material.dart';
import '../stream/word_stream.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {

  ImageLinkStream imageStream = ImageLinkStream();
  String temp = "";
  final List<ImageModel> items = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stream Words',
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Image.network(items[index].url ?? ""),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Press!'),
          onPressed: () {
            changeImage();
          },
        ),
      ),
    );
  }

  changeImage() async {
    imageStream.getImageLink().listen((eventImageLink) {
      setState(() {
        print(eventImageLink);
        items.add(eventImageLink!);
      });
    });
  }



}
